# Source manjaro-zsh-configuration
if [[ -e /usr/share/zsh/manjaro-zsh-config ]]; then
  source /usr/share/zsh/manjaro-zsh-config
fi
# Use manjaro zsh prompt
if [[ -e /usr/share/zsh/manjaro-zsh-prompt ]]; then
  source /usr/share/zsh/manjaro-zsh-prompt
fi

# Path to your oh-my-zsh installation.
export ZSH="/home/dbarta/.oh-my-zsh"

ZSH_THEME="gozilla"

source $ZSH/oh-my-zsh.sh

# ----- User aliases -----

alias !='sudo'
alias ls='lsd --group-dirs first'
alias ll='lsd -l --group-dirs first'
alias l='lsd -l --group-dirs first'
alias la='lsd -la --group-dirs first'
alias d='dirs -v | head -10'
alias usage='du -h -d1' # shows disk usage of current directory

# Clear alias
alias c='clear'

# VPN aliases
alias nst='nordvpn status'
alias ncn='nordvpn connect'
alias ndcn='nordvpn disconnect'

# Xampp and Tomcat (7)
alias startOnlyXampp='sudo /opt/lampp/lampp start'
alias stopOnlyXampp='sudo /opt/lampp/lampp stop'
alias startTomcat='sudo systemctl start tomcat7'
alias stopTomcat='sudo systemctl stop tomcat7'
alias startXamppAndTomcat='sudo /opt/lampp/lampp start&&sudo systemctl start tomcat7'
alias stopXamppAndTomcat='sudo /opt/lampp/lampp stop&&sudo systemctl stop tomcat7'

# Generating ssh key
alias ssh-agent='eval $(ssh-agent)'

# Removes uninstalled (and unused) packages to save space
alias remove-uninstalled-cached-packages='sudo paccache -ruk0'

# Screen layout (arandr)
alias layout-multi='.screenlayout/home_layout_multi_screen.sh' # layout for multiple screens
alias layout-laptop='.screenlayout/home_layout_laptop_screen.sh' # layout for laptop screen only

# Bluetooth section
alias bt='bluetoothctl'
alias bt-on='bluetoothctl power on'
alias bt-off='bluetoothctl power off'
alias cn-AKG_Y50BT='bluetoothctl power on && bluetoothctl connect 34:DF:2A:3C:75:70'
alias cn-Sony_SRS_X3='bluetoothctl power on && bluetoothctl connect 0C:A6:94:83:7A:B3'
alias cn-WF_1000='bluetoothctl power on && bluetoothctl connect 38:18:4C:C1:D8:0E'

# Wifi and internet section
alias int='nmtui'
alias intoff='nmcli networking off'
alias inton='nmcli networking on'

# Apps shortcuts
alias o='okular'

# Other aliases
alias runp='lsof -i'
alias v='vim'

# System update section
alias u='sudo pacman -Syyuu'

# Installing and uninstalling packages
alias ins='makepkg -si'
alias install='sudo pacman -S'
alias remove='sudo pacman -R'
alias remove-force='sudo pacman -Rdd'

# Backup system
alias backup='sudo rsync -aAXvP --delete / --exclude={"/dev/*","/proc/*","/sys/*","/tmp/*","/run/*","/mnt/*","/media/*","/lost+found","/home/dbarta/Downloads/*","/home/dbarta/.cache/*"} /run/media/HDD/Backups/Backup-David-Notebook-Linux'

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
