set nu
set ai
set tabstop=4
set ls=2
set autoindent
set mouse=a
syntax on

highlight LineNr term=bold cterm=NONE ctermfg=DarkGrey ctermbg=NONE gui=NONE
